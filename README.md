# narray

TODO: Write a description here

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  narray:
    github: your-github-user/narray
```

## Usage

```crystal
require "narray"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://github.com/your-github-user/narray/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [your-github-user](https://github.com/your-github-user) NIFR91 - creator, maintainer
