class Narray(T)

  # Gives a new shape by args to the narray without changing its data
  # 
  def reshape(*args) 
    reshape(args)
  end 
  
  # Reshapes the narray to the 1D 
  #
  def flatten
    reshape({@size})
  end 

  # Gives a new shape to the narray without changing its data 
  # 
  # The new shape should be compatible with the original shape.
  #
  def reshape(newshape) 
    cnt = 1_i64
    newshape.size.times{|i| cnt *= newshape[i] } 
    if @size != 0 && cnt != @size
      raise "cannot reshape array of size #{size} unto shape #{newshape}" 
    end 

    if (newdim = newshape.size.to_i64 ) != @dim 
      @dim   = newshape.size.to_i64
      @shape = @shape.realloc(@dim)
      @stride = @stride.realloc(@dim) 
      @indexes = @indexes.realloc(@dim)
    end 

    @dim.times{|k| @shape[k] = newshape[k].to_i64 } 

    @dim.times do |k| 
      s = 1_i64
      (k+1).upto(@dim-1).each do |j|
        s *=  @shape[j]
      end 
      @stride[k] = s
    end  

    self
  end 


  # Append multople values. The same as `push, bit takes an arbitrary number 
  # of values to push into `self`. Returns `self`
  #
  def push(*values : T) 
    values.size.times do |i|
      self.push values[i]
    end 
    self 
  end  
  
  # Append. Pushes one value to the end of `self`, given that the type of the 
  # value is *T*. This methods returns `self so several calls can be chained
  #
  def push(value : T)
    check_needs_resize
    raise "to push a value it must be a flat array" if @dim != 1
    @buffer[@size] = value
    @size += 1
    @shape[0] += 1
    self  
  end 

  # Alias to push 
  #
  def <<(value) 
    self.push(value)
  end 


  # Removes the last value from `self` returns blocks value if self is empty
  #
  def pop 
    raise "to push a value it must be a flat array" if @dim != 1
    if @size == 0
      yield 
    else 
      @size -= 1
      @shape[0] -= 1
      value = @buffer[@size]
      (@buffer + @size).clear 
      value
    end 
  end  

  # Like `pop`, but returns `nil`if self is empty 
  # 
  def pop?
    pop{nil}
  end 

  # Removes the last value from `self`, at index -1
  #
  def pop
    pop{raise IndexError.new} 
  end 

  # Removes the last `n` values from `self` , at index -1
  # 
  def pop(n : Int64)
    return Narray(T).new if n > @size
    a = Narray(T).new(n)
    nz = n - 1
    n.times{|i| a[nz-i] = self.pop } 
    a
  end 

  # Removes all elements from self
  #
  def clear
    @buffer.clear(@size)
    @size = 0_i64
    self.flatten
    self
  end



  private def check_needs_resize
    double_capacity if @size == @capacity
  end 

  private def double_capacity
    resize_to_capacity(@capacity == 0 ? 3_i64 : (@capacity * 2) ) 
  end 

  private def resize_to_capacity(capacity) 
    @capacity = capacity 
    if @buffer 
      @buffer = @buffer.realloc(@capacity)
    else 
      @buffer = Pointer(T).malloc(@capacity)
    end 
  end 

end 
