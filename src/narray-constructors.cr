class Narray(T)

  # Creates a new empty Narray
  # 
  def initialize 
    @size     = 0_i64
    @capacity = 0_i64
    @buffer   = Pointer(T).null
    @dim      = 1_i64
    @indexes  = Pointer(Int64).malloc(1)
    @shape    = Pointer(Int64).malloc(1,0)
    @stride   = Pointer(Int64).malloc(1,1)
  end 

  # Creates a new #Narray of size 'size' with all bytes initialized to 0
  #
  def initialize(size : Int)
    @size     = size.to_i64
    @capacity = @size
    @buffer   = Pointer(T).malloc(@size)
    @dim      = 1_i64
    @shape    = Pointer(Int64).malloc(1,@size) 
    @stride   = Pointer(Int64).malloc(1,1)
    @indexes  = Pointer(Int64).malloc(@dim,1)
  end

  # Creates a new #narray and invokes the given block once for each 
  # index of 'self', assigning the block's value in that index
  #
  # ```
  # Narray.new(4){|i| i} #=> [@shape [4], @size 4, [0,1,2,3]]
  # ```
  def self.new(size : Int, &block : Int64 -> T) 
    nary = Narray(T).new(size) 
    nary.size.times do |i|
      nary[i] = yield i
    end 
    nary 
  end 

  # Creates a new #Narray with the specified shape and invokes the 
  # given block once for each indices, assigning the block's value in that 
  # indices's index. 
  #
  # ```
  # Narray.new(2,2){|(i,j)| {i,j}} #=> [@shape:[2,2],@size:4,[{0,0},{0,1},{1,0},{1,1}]
  # ```
  #
  def self.new(*args, &block : Narray(Int64) -> T) 
    Narray.new(args){|narray| yield  narray } 
  end 

  # Creates a new #Narray with the specified shape and invokes the 
  # given block once for each indices, assigning the block's value in that 
  # indices's index. 
  #
  # ```
  # Narray.new([2,2]){|(i,j)| {i,j}} #=> [@shape:[2,2],@size:4,[{0,0},{0,1},{1,0},{1,1}]
  # ```
  #
  def self.new(shape, &block : Narray(Int64) -> T) 
    size = shape.size.times.reduce(1_i64){|acc,i| acc * shape[i]} 
    nary = Narray(T).new(size).reshape(shape)
    ptr = nary.to_unsafe
    offset = 0_i64
    nary.indexes.each do |indexes| 
      ptr[offset] = yield indexes 
      offset += 1
    end  
    nary
  end 

  # Creates a new #Narray from an #Array like object in row-order
  #
  # ```
  # Narray.new [[1,2],[3,4]] #=> [@shape: [2,2], @size 4, [1,2,3,4]]
  # ```
  # 
  def self.new(array : Array) : Narray 
    FlattenHelper(typeof(FlattenHelper.element_type(array))).flatten(array) 
  end 

  # Creates a #Narray from an enumerable object
  #
  def self.new(iter :  Iterable(U) | Enumerable(U)) forall U 
    raise "for empty arrays use 'Narray(ElementType).new'" if U == NoReturn
    nary = Narray(U).new
    iter.each{|e| nary << e } 
    nary
  end 

  # Creates a #Narray from given arguments 
  #
  # ```
  # Narray[1,2,3]       #=> [@shape: [3], @size 3, [1,2,3]] 
  # Narray[[1,2],[3,4]] #=> [@shape: [2,2], @size: 4, [1,2,3,4]]
  # ```
  def self.[](*args)
    Narray.new(args.to_a)
  end 

  # Creates an empty #Narray of the specified type 
  #
  # ```
  # nary = Narray[Int32] #=> [@shape: [0], @size 0, [ ]]
  # nary << 1 << 2 << 3  #=> [@shape: [3], @size 3, [1,2,3]]
  # ```
  #
  def self.[](cls : T.class)
    Narray(T).new 
  end 


  # Module for creating a Narray form an Array or Iterable 
  # object 
  #
  private module FlattenHelper(T)

    # Creates the Narray from the array
    #
    def self.flatten(ary : Array)
      result = Narray(T).new 
      flatten ary, result
      result.reshape (get_shape(ary)  || Narray[result.size] )
    end 
    
    # Recursively gets the elements of the arrays 
    #
    private def self.flatten(ary : Array, result : Narray(T))
      ary.each{|element| flatten element, result } 
    end 
  
    # Base case for the flatten recursion
    #
    private def self.flatten(other : T, result : Narray(T))
      result << other
    end 
   
    # Recursively gets the element type of the arrays 
    #
    def self.element_type(ary)
      case ary
      when Array then element_type(ary.first)
      else ary
      end 
    end 

    # Recursively gets the shape of the array, it only handles full 
    # ndimentional arrays if there's a mismatch in the dimensions it 
    # returns `nil`
    # 
    # ```
    # FlattenHelper(Int32).get_shape([[1,2,3]]) #=> [1,3]
    # FlattenHelper(Int32).get_shape([1,[1,2,3]) #=> [-1]
    # ```
    #
    def self.get_shape(ary, shape = Narray(Int64).new,error = false) 
      return shape unless ary.is_a?(Array)
      return shape if ary.empty?

      all_array = true
      all_elements = true 
      all_same_size = true 
      size = nil 

      ary.each do |element| 
        if element.is_a?(Array) && all_same_size
          all_elements = false 
          if size 
            all_same_size = (element.size == size) 
          else 
            size = element.size.to_i64 
          end 
        else 
         all_array = false 
        end 
      end  
      
      if (!all_array && !all_elements) || !all_same_size
        return nil
      else 
        shape << ary.size.to_i64
      end 
      
      get_shape ary[0],shape
    end 
  end 
end


