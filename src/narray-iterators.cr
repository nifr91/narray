class Narray(T)

  # Iterator that returns each element of the 1d memory block of 
  # the narray
  #
  def elements
    return ElementsIterator.new(self)
  end 

  # Iterator over the indexes of the narray given by its shape. 
  # 
  # The order is by row, example : 
  #
  # ```
  # [[[1,2],  
  #  [3,4]] , 
  #  [[5,6],
  #   [7,8]]]
  # ``` 
  # gives :
  # 
  # ``` 
  # [0,0,0] #=> 1
  # [0,0,1] #=> 2
  # [0,1,0] #=> 3
  # [0,1,1] #=> 4
  # [1,0,0] #=> 5
  # [1,0,1] #=> 6
  # [1,1,0] #=> 7
  # [1,1,1] #=> 8
  # ```
  # 
  def indexes 
    IndexIterator.new(self.shape)
  end 



  module Iterator(T)
    
    def each 
      self 
    end 

    def each : Nil 
      loop do 
        value = self.next 
        break if value.is_a?(Stop)
        yield value
      end 
    end     
    
    def stop
      Iterator.stop
    end 

    def self.stop
      Stop::INSTANCE
    end 

    class Stop
      INSTANCE = new
    end 

  end 


  private class ElementsIterator(T)
    include Iterator(T)
    
    def initialize(@elements : Narray(T))
      @pos = 0_i64
    end 

    def next 
      return stop if @pos == @elements.size 
      val  = @elements[@pos]
      @pos += 1
      return val  
    end 

    def rewind
      @pos = 0_i64
    end 
  end 

  private class IndexIterator(T)
    include Iterator(T)

    @dim : Int64
    def initialize(@shape : T)
      @dim = @shape.size.to_i64 - 1_i64
      @indexes = Narray(Int64).new(@shape.size){0_i64}
      @bounds  = Narray(Int64).new(@shape.size){|i| @shape[i] - 1_i64} 
      @indexes[-1] = -1_i64
      @i = 0_i64
      @pos = @dim
    end 

    def next 
      while @pos >= 0 
        @i = @indexes[@pos] 
        break if @i < @bounds[@pos]
        @indexes[@pos] = 0_i64
        @pos -= 1
      end 
      return stop if @i >= @bounds[@pos]
      @i += 1
      @indexes[@pos] = @i 
      @pos = @dim 
      return @indexes   
    end 
  end 


end 
