class Narray(T) 
 

  # Returns the element at the given index 
  # Negative indices can be used to start counting from the end of the array. 
  # Raises IndexError if trying to access an element outside the array's range
  #
  def [](index : Int)
    index = check_index_in_bounds(index){ raise IndexError.new} 
    @buffer[index]      
  end 

  # Returns the element at the given *index* 
  # Negative indices can be used to start counting from the end of the array 
  # Returns `nil` if trying to access and element outside the array's range.
  def []?(index : Int) 
    return nil unless index = check_index_in_bounds(index){nil} 
    @buffer[index]
  end

  # Returns the element at the given n-dim arguments
  #
  # Negative indexes can be used to start counting from the end of each 
  # dimention.
  #
  # Raises IndexError if trying to access an element outside the valid 
  # narray boundaries
  #
  def [](*indexes)
    self[indexes] 
  end 

  # Returns the element at the given n-dim indexable object
  #
  # Negative indexes can be used to start counting from the end of each 
  # dimention.
  #
  # Raises IndexError if trying to access an element outside the valid 
  # narray boundaries
  #
  def [](indexes)
    indexes = check_indexes_in_bounds(indexes){ raise IndexError.new } 
    @buffer[offset(indexes)]
  end 

  # Returns the element at the given n-dim indexable object
  #
  # Negative indexes can be used to start counting from the end of each 
  # dimention.
  #
  # Returns `nil` if trying to access an element outside the valid 
  # narray boundaries
  #
  def []?(indexes) 
    return nil unless indexes = check_indexes_in_bounds(indexes){nil}
    @buffer[offset(indexes)] 
  end 

  # Returns the element at the given n-dim indexable object
  #
  # Negative indexes can be used to start counting from the end of each 
  # dimention.
  #
  # Returns `nil` if trying to access an element outside the valid 
  # narray boundaries
  #
  def []?(*indexes) 
    self[indexes]?
  end 

  # Sets the given value at the given index 
  # 
  # Negative indices can be used to start counting from the end of the 
  # narray.
  # Raises `IndexError`if trying to set an element outside the narray's range
  #
  def []=(index : Int,value : T)
    index = check_index_in_bounds(index){raise IndexError.new } 
    @buffer[index] = value
  end 


  # Sets the given value at the given n-dimentional indexes
  #
  # Negative indices can be used to start counting from the end of the narray.
  # Raises `IndexError` if trying to set an element outside the narray's range
  #
  def []=(indexes,value : T) 
    indexes = check_indexes_in_bounds(indexes){raise IndexError.new } 
    @buffer[offset(indexes)] = value
  end 


  # Sets the given value at the given n-dimentional indexes given at the args
  #
  # Negative indices can be used to start counting from the end of the narray.
  # Raises `IndexError` if trying to set an element outside the narray's range
  #
  def []=(*indexes)
    value = indexes.last
    raise IndexError.new if (indexes.size - 1) != @dim 
    indexes = check_indexes_in_bounds(indexes){raise IndexError.new}
    @buffer[offset(indexes)] = value
  end 

  # Returns the offset of the n-dimentional index (n_0,n_1,...,n_{N-1})
  #
  private def offset(indexes) 
    offset = 0_i64
    @dim.times do |d|
      offset += @stride[d]* indexes[d]
    end 
    offset 
  end 

  # Returns the positive indexes if all indexes are inside the bounds of the 
  # array  (negative or positive) or the yielded value otherwise
  #
  private def check_indexes_in_bounds(indexes) 
    return yield if indexes.size < @dim
    @dim.times do |d|
      val = indexes[d].to_i64 
      @indexes[d] = (val < 0) ? (val + @shape[d]) : val 
      return yield unless (0 <= @indexes[d] < @shape[d])
    end 
    @indexes
  end

  # Returns the positive index if the index is inside the bounds of the array 
  # (negative or positive) or the yielded value otherwise
  #
  private def check_index_in_bounds(index) 
    index += @size if index < 0 
    if 0 <= index < size
      index 
    else 
      yield 
    end 
  end 

end 
