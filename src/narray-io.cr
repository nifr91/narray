class Narray(T)

  def inspect(io)
    io << '['
            io << "@shape:"<<  self.shape << ',' << ' '
            io << "@size:" << self.size << ',' << ' '
    # io << '['
    # if @size > 0
    #   (@size - 1_i64).times do |i|
    #     io << @buffer[i] << ','
    #   end 
    #   io << @buffer[@size - 1_i64]
    # end 
    # io << ']'
    to_s(io) 

    io << ']'
  end 

  def to_s
    String.build { |str| to_s str } 
  end 

  def to_s(io)
    return (io << '[' << ']' ) if @size == 0 
    recstr(io) 
    io 
  end

  def recstr(io,dim = 0_i64,indexes = Narray.new(@dim){0_i64})
    if dim == @dim 
      io.print  self[indexes] 
      return 
    end   
    io.print '['
    @shape[dim].times do |nk|
      indexes[dim] = nk 
      recstr(io,dim+1, indexes)
      io.print ',' unless nk == @shape[dim]-1
    end 
    io.print ']'
 end  

end 

