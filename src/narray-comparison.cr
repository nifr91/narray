class Narray(T)

  # Equality of self and other in view of numerical array, 
  # (both arrays have same shape and corresponding elements are equal). 
  #
  # ```
  # [1,2,3] == [1,2,3]             #=> true
  # [1,2,3] == [1.0,2,3_i64]       #=> true 
  # [[1,2],[3,4]] == [[1,2],[3,4]] #=> true 
  # [[1,2],[3,4]] == [1,2,3,4]     #=> false
  # [1] == [2]                     #=> false
  # [1] == [1,2]                   #=> false
  # ```
  #
  def ==(other : Narray)
    return false if @size != other.size
    return false if @dim != other.dim 
    other_shape = other.shape
    @dim.times  {|i| return false if @shape[i]  != other_shape[i]} 
    @size.times {|i| return false if @buffer[i] != other[i] } 
    return true 
  end 

  # Element-wise equality comparison with narray
  #
  def eq(other : Narray) 
    shape = self.shape  
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] == other[i]}.reshape(shape)
  end  

  # Element-wise equality comparison with value
  #
  def eq(other) 
    Narray.new(@size){|i| self[i] == other}.reshape(shape)
  end

  # Element-wise comparison with  narray
  #
  def <=>(other : Narray) 
    shape = self.shape 
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] <=> other[i]}.reshape(shape)
  end 

  # Element-wise comparison with value 
  def <=>(other) 
    Narray.new(@size){|i| self[i] <=> other}.reshape(shape)
  end 


  # Compares this object to other based on #<=>, returning true if -1
  #
  def <(other : Narray)
    shape = self.shape 
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] < other[i]}.reshape(shape)
  end 

  # Compares this object to other based on #<=>, returning true if -1
  #
  def <(other) 
    Narray.new(@size){|i| self[i] < other}.reshape(shape)
  end 



  # Compares this object to other based on #<=>, returning true if -1 or 0
  #
  def <=(other : Narray)
    shape = self.shape 
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] <= other[i]}.reshape(shape)
  end 

  # Compares this object to other based on #<=>, returning true if -1 or 0
  #
  def <=(other) 
    Narray.new(@size){|i| self[i] <= other}.reshape(shape)
  end 


  # Compares this object to other based on #<=>, returning true if 1
  #
  def >(other : Narray)
    shape = self.shape 
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] > other[i]}.reshape(shape)
  end 

  # Compares this object to other based on #<=>, returning true if 1
  #
  def >(other) 
    Narray.new(@size){|i| self[i] > other}.reshape(shape)
  end 


  # Compares this object to other based on #<=>, returning true if 1 or 0 
  #
  def >=(other : Narray)
    shape = self.shape 
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] >= other[i]}.reshape(shape)
  end 

  # Compares this object to other based on #<=>, returning true if 1 or 0 
  #
  def >=(other) 
    Narray.new(@size){|i| self[i] >= other}.reshape(shape)
  end 


  # Element-wise bitwise AND of self and other
  def &(other : Narray)
    shape = self.shape 
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] & other[i]}.reshape(shape)
  end 

  # Element-wise bitwise AND of self and other
  def &(other)
    Narray.new(@size){|i| self[i] & other}.reshape(shape)
  end 

  # Element-wise bitwise XOR of self and other
  def ^(other : Narray) 
    shape = self.shape 
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] ^ other[i]}.reshape(shape)
  end

  # Element-wise bitwise XOR of self and other
  def ^(other)
    Narray.new(@size){|i| self[i] ^ other}.reshape(shape)
  end 


  # Element-wise bitwise OR of self and other
  def |(other : Narray)
    shape = self.shape 
    raise "incompatible shapes for comparison" unless (shape == other.shape) 
    Narray.new(@size){|i| self[i] | other[i]}.reshape(shape)
  end 

  # Element-wise bitwise OR of self and other
  def |(other)
    Narray.new(@size){|i| self[i] | other}.reshape(shape)
  end 

end 
