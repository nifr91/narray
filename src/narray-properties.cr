class Narray(T)
  # The number of total elements
  #
  getter size 

  # The dimension of the narray (number of indexes) 
  #
  def dim 
    self.shape.size
  end 

  # Narray with the number of elements along each dimention
  #
  def shape
    Narray(Int64).new(@dim) { |i| @shape[i]}
  end 

  # Narray with the number of locations in memory between beginnings of 
  # successive elements.
  #
  def stride 
    Narray(Int64).new(@dim){|i| @stride[i] }
  end 

  # Pointer to the memory block 
  # 
  def to_unsafe 
    @buffer
  end 
  
  # Is true if there's no elements in the narray, false otherwise
  #
  def empty? 
    @size == 0
  end 
end 


