require "./spec_helper.cr"


describe "[]" do 
  it "gets on positive index" do 
    Narray[1,2,3][1].should eq 2 
  end 
  
  it "gets on negative index" do 
    Narray[1,2,3][-1].should eq 3
  end 


  it "raises on index out of bounds" do 
    expect_raises IndexError do 
      Narray[1,2,3][3]
    end 
    expect_raises IndexError do 
      Narray[1,2,3][-4]
    end 
  end 

  it "gets nilable if invalid index" do 
    Narray[1,2,3][2]?.should eq 3 
    Narray[1,2,3][3]?.should be_nil
  end 


  it "gets on positive n-dim indexes" do 

    a = Narray[[1,2],[3,4]]
    
    a[1,1].should eq 4
    a[0,1].should eq 2
    a[0,0].should eq 1
  end 

  it "gets on negative n-dim indexes" do 
    a = Narray[[1,2],[3,4]] 

    a[-1,-1].should eq 4
    a[0,-1].should eq 2
    a[-2,-2].should eq 1
  end


  it "gets nilable if invalid indexes" do 
    Narray[[1,2],[3,4]][0,1]?.should eq 2 
    Narray[[1,2],[3,4]][0,2]?.should be_nil
  end 



  it "raises on indexes out of bounds" do 
    a = Narray[[1,2],[3,4]] 
    
    expect_raises IndexError do 
      a[2,2]
    end 

    expect_raises IndexError do 
      a[-4,-3]
    end 
  end  

end 


describe "[]=" do 
  it "sets on positive index" do 
    a = Narray[1,2,3]
    a[1] = 4
    a[1].should eq 4
  end 

  it "sets on negative index" do 
    a = Narray[1,2,3]
    a[-1] = 4
    a[2].should eq 4
  end 


  it "raises on index out of bounds" do 
    a = Narray[1,2,3]
    expect_raises IndexError do 
      a[4] = 4
    end 
  end 

  it "sets on positive nd-indexes" do 
    a = Narray[[1,2],[3,4]] 
    a[1,1] = 10
    a[1,1].should eq 10 
  end 

  it "sets on negative nd-indexes" do 
    a = Narray[[1,2],[3,4]] 
    a[-1,-1] = 10
    a[-1,-1].should eq 10 
  end 

  it  "raises on indexes out of bounds" do
    a = Narray[[1,2],[3,4]] 
    expect_raises IndexError do 
      a[-1,-3] = 10
    end 
  end 

end 
