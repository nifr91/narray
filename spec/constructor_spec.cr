require "./spec_helper.cr"

describe "constructor" do 

  describe "new" do 
    it "should create an empty narray" do 
      actual = Narray(Int32).new
      actual.size.should      eq 0
      actual.dim.should       eq 1
      actual.to_unsafe.should eq Pointer(Int32).null
      actual.shape.should     eq Narray[0_i64]
      actual.stride.should    eq Narray[1_i64]
    end  

    it "should create an narray initialized to 0" do 
      actual = Narray(Char).new(4)
      actual.should eq Narray['\0','\0','\0','\0']
      expected  =Narray['\0','\0','\0','\0']   

      actual = Narray(Int32).new(4)
      actual.should eq Narray[0,0,0,0]
    end 
    
    it "creates an narray initialized to value" do 
      actual = Narray.new(2){1.0}
      actual.should eq Narray[1.0,1.0]

      actual = Narray.new(3){""}
      actual.should eq Narray["","",""]

      nary = Narray[1] 
      actual = Narray.new(3){nary}
      actual.should eq Narray[nary,nary,nary]
      actual[0].should be nary
    end 

    it "creates an narray with given shape" do 
      actual = Narray.new(2,2){0} 
      actual.should eq Narray[[0,0],[0,0]]  

      shape = Narray[2,2]
      actual = Narray.new(2,2) do |(i,j)|
        (i+j).to_f64
      end 
      actual.should eq Narray[[0.0,1.0],[1.0,2.0]]

      shape = Narray[2,2,2]
      
      actual = Narray.new(shape) do |(i,j,k)|
        (i == j == k ) ? 1 : 0 
      end 
      actual.should eq Narray[[[1,0],[0,0]],[[0,0],[0,1]]]
    end 


    it "creates an naray from a array-like object" do 
      actual = Narray.new [1,2,3]
      actual.should eq Narray[1,2,3]

      actual = Narray.new [[1,2],[3,4]]
      actual.should eq Narray[[1,2],[3,4]]
 
    end 

    it "creates an narray from an enumerable object" do 
      tuple = {1.0,2.0,3.0} 
      iterable = {1.0,2.0,3.0}.each 
      set   = tuple.to_set

      actual = Narray.new(set)
      actual.should eq Narray[1.0,2.0,3.0]

      actual = Narray.new(iterable)
      actual.should eq Narray[1.0,2.0,3.0]


      actual = Narray.new(tuple)
      actual.should eq Narray[1.0,2.0,3.0]
    end 

  end 

  describe "Narray[]" do 

    it "should create an emtpy narray" do 
      actual = Narray[Int32]
      actual.should eq Narray(Int32).new 
      actual.should be_a Narray(Int32)
    end 

    it "should create an array with given args" do 
      actual = Narray.new [1,2,3,4] 
      actual.should eq Narray[1,2,3,4]
    end 
    
    it "handles array with wrong dimentions by flattening it" do 
      actual = Narray[[1,[1]],[1,2,3]]
      actual.shape.should eq Narray[5]
      actual.to_s.should eq "[1,1,1,2,3]"

      actual = Narray[[1,2,3],1]
      actual.shape.should eq Narray[4]
      actual.to_s.should eq "[1,2,3,1]"
    end 
  end 

end 
