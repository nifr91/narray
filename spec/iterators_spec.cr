require "./spec_helper.cr"

describe "elements iterator" do 
  it "does next" do 
    a = Narray[1,2,3]
    iter = a.elements

    iter.next.should eq 1
    iter.next.should eq 2
    iter.next.should eq 3
    iter.next.should be_a Narray::Iterator::Stop

    iter.rewind
    iter.next.should eq 1
  end  
end 


describe "indexes iterator" do 
  it "does next" do 
    a = Narray[[[1,2],[3,4]],[[5,6],[7,8]]]
    
    iter = a.indexes 

    iter.next.should eq Narray[0,0,0] 
    iter.next.should eq Narray[0,0,1]
    iter.next.should eq Narray[0,1,0] 
    iter.next.should eq Narray[0,1,1] 
    iter.next.should eq Narray[1,0,0] 
    iter.next.should eq Narray[1,0,1] 
    iter.next.should eq Narray[1,1,0] 
    iter.next.should eq Narray[1,1,1] 
    
    iter.next.should be_a Narray::Iterator::Stop 
  end 
end 
