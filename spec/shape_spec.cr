require "./spec_helper.cr"

describe "reshape" do 

  it "assigns the new shape to the narray" do 
    a = Narray.new(8){|i| i} 

    a.reshape(2,2,2).should eq Narray[[[0,1],[2,3]],[[4,5],[6,7]]]
    a.reshape([1,8]).should eq Narray[[0,1,2,3,4,5,6,7]]
    a.reshape({8,1}).should eq Narray[[0],[1],[2],[3],[4],[5],[6],[7]]
  end 

  it "raises on invalid shape" do 
    expect_raises Exception do 
      Narray.new(2){0}.reshape(2,2)
    end 
  end 

end 


describe "clear" do 
  it "removes all elements from self" do 
    a = Narray[1,2,3]
    a.clear 
    a.should eq Narray[Int32]
  end 
end 

describe "push" do 
  
  it "appends the given value to the 1-d memory block" do 
    a = Narray[1,2] 
    a.push(3).should be a
    a.should eq Narray[1,2,3]
  end 

  it "should array if appending to a non flat narray" do 
    expect_raises Exception do 
      Narray[[1]].push 1
    end 
  end 

  it "pushes multiple elements" do 
    a = Narray[1,2]
    a.push(3,4).should be a 
    a.should eq Narray[1,2,3,4]
  end 

  it "pushes multiple elements to an empty narray" do 
    a = Narray[Int32]
    a.push(1,2,3).should be a
    a.should eq Narray[1,2,3]
  end 

  it "has the << alias" do 
    a = Narray[1,2]
    a << 3
    a.should eq Narray[1,2,3] 
  end 
end 


describe "pop" do 
  it "pops when non empty" do 
    a = Narray[1,2,3] 
    a.pop.should eq 3
    a.should eq Narray[1,2]
  end 

  it "pops many elements" do 
    a = Narray[1,2,3,4,5]
    b = a.pop(3)
    b.should eq Narray[3,4,5]
    a.should eq Narray[1,2]
  end 

  it "returns nil if no elements are left" do 
    a = Narray[1,2,3,4,5]
    _ = a.pop(5) 
    b = a.pop?
    b.should be_nil
  end 

end 


describe "flatten" do 
  it "reshapes to a 1d array" do 
    a = Narray[[1,2],[3,4]] 
    a.flatten.should eq Narray[1,2,3,4]
  end 
end 
