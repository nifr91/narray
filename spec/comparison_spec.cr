require "./spec_helper.cr"

describe "==" do 
  
  it "returns true if a is equal to b" do 
    (Narray[1] == Narray[1]).should be_true 
  end

  it "returns false if a is not equal to b" do 
    (Narray[1] == Narray[2]).should be_false
  end 

  it "returns true if a[i] == a[j] forall i,j such that i == j" do 
    (Narray[1,2] == Narray[1.0,2.0]).should be_true
  end 

  it "is false if a.shape != b.shape" do 
    (Narray[[1,2],[3,4]] == Narray[1,2,3,4]).should be_false
  end 

  it "is false if a.size != b.size" do 
    (Narray[1,2,3] == Narray[1,2]).should be_false 
  end 

end 

describe "eq" do 

  it "returns a boolean narray" do 
    (Narray[1].eq Narray[2]).should be_a Narray(Bool)
  end 

  it "performs a element-wise comparison" do
    (Narray[1,2,3].eq Narray[1.0,2.0,5]).should eq Narray[true,true,false] 
    (Narray[[1,2],[3,4]].eq Narray[[1,4],[5,6]]).should eq Narray[[true,false],[false,false]]
  end 

  it "compares all elements to value" do 
    (Narray[1,2,3].eq 3).should eq Narray[false,false,true]
  end 
end 


describe "<=>" do 
  it "returns a int32 narray" do 
    (Narray[1] <=> Narray[2]).should be_a Narray(Int32)
  end 

  it "returns 0 if equal" do 
    (Narray[1,1] <=> Narray[1,1]).should eq Narray[0,0]
  end 

  it "returns 1 if greater" do
    (Narray[[1,1]] <=> Narray[[-1,0]]).should eq Narray[[1,1]] 
  end 

  it "returns -1 if lesser" do 
    (Narray[[1],[1]] <=> Narray[[2],[3]]).should eq Narray[[-1],[-1]]
  end 

  it "compares all elements to value" do 
    (Narray[2,3,4] <=> 3).should eq Narray[-1,0,1]
  end 

end 

describe "<" do 
  it "performs a element-wise comparison" do 
    (Narray[[1],[2]] < Narray[[1],[2]]).should eq Narray[[false],[false]]
    (Narray[[0],[1]] < Narray[[1],[2]]).should eq Narray[[true],[true]]
    (Narray[[1],[1]] < Narray[[1],[2]]).should eq Narray[[false],[true]]
  end 

  it "compares all elements to value" do 
    (Narray[2,3,4] < 3).should eq Narray[true,false,false]
  end 
end 

describe "<=" do 
  it "performs a element-wise comparison" do 
    (Narray[[1],[2]] <= Narray[[1],[2]]).should eq Narray[[true],[true]]
    (Narray[[0],[1]] <= Narray[[1],[2]]).should eq Narray[[true],[true]]
    (Narray[[1],[1]] <= Narray[[1],[2]]).should eq Narray[[true],[true]]
  end 

  it "compares all elements to value" do 
    (Narray[2,3,4] <= 3).should eq Narray[true,true,false]
  end 

end

describe ">" do 
  it "performs a element-wise comparison" do 
    (Narray[[1],[2]] > Narray[[1],[2]]).should eq Narray[[false],[false]]
    (Narray[[2],[1]] > Narray[[1],[2]]).should eq Narray[[true],[false]]
    (Narray[[1],[2]] > Narray[[0],[1]]).should eq Narray[[true],[true]]
  end 
  it "compares all elements to value" do 
    (Narray[2,3,4] > 3).should eq Narray[false,false,true]
  end 


end 

describe ">=" do 
  it "performs a element-wise comparison" do 
    (Narray[[1],[2]] >= Narray[[1],[2]]).should eq Narray[[true],[true]]
    (Narray[[2],[1]] >= Narray[[1],[2]]).should eq Narray[[true],[false]]
    (Narray[[1],[2]] >= Narray[[0],[1]]).should eq Narray[[true],[true]]
  end 

  it "compares all elements to value" do 
    (Narray[2,3,4] >= 3).should eq Narray[false,true,true]
  end 
end


describe "&" do 
  it "performs a integer element-wise bit's bitwise AND" do 
    (Narray[[0b1111,0b0101]] & Narray[[0b0101,0b0100]])
      .should eq Narray[[0b0101,0b0100]]
  end 

  it "performs a boolean element-wise AND" do 
    (Narray[[true,true,false,false]] & Narray[[true,false,true,false]])
      .should eq Narray[[true,false,false,false]]
  end 

  it "element-wise bits bitwise AND with integer value" do 
    (Narray[0b1111,0b0101] & 0b0101).should eq Narray[0b0101,0b0101]
  end 

  it "element-wise bits bitwise AND with boolean value" do 
    (Narray[true,false] & true).should eq Narray[true,false]
  end 


end 

describe "^" do 
  it "performs a integer element-wise bit's bitwise XOR" do 
    (Narray[[0b1111,0b0101]] ^ Narray[[0b0101,0b0100]])
      .should eq Narray[[0b1010,0b0001]]
  end 
  it "performs a boolean element-wise XOR" do 
    (Narray[[true,true,false,false]] ^ Narray[[true,false,true,false]])
      .should eq Narray[[false,true,true,false]]
  end 

  it "element-wise bits bitwise XOR with integer value" do 
    (Narray[0b1111,0b0101] ^ 0b0101).should eq Narray[0b1010,0b0000]
  end 

  it "element-wise bits bitwise XOR with boolean value" do 
    (Narray[true,false] ^ true).should eq Narray[false,true]
  end 


end 

describe "|" do 
  it "performs a integer element-wise bit's bitwise OR" do 
    (Narray[[0b1111,0b0101]] | Narray[[0b0101,0b0100]])
      .should eq Narray[[0b1111,0b0101]]
  end 
  it "performs a boolean element-wise OR" do 
    (Narray[[true,true,false,false]] | Narray[[true,false,true,false]])
      .should eq Narray[[true,true,true,false]]
  end 

  it "element-wise bits bitwise OR with integer value" do 
    (Narray[0b1111,0b0101] | 0b0101).should eq Narray[0b1111,0b0101]
  end 

  it "element-wise bits bitwise OR with boolean value" do 
    (Narray[true,false] | true).should eq Narray[true,true]
  end 

end 
