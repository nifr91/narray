require "./spec_helper.cr"

describe "size" do 
  it "returns the number of total elements in the narray" do 
    Narray[1,2,3].size.should eq 3 
  end 
end 

describe "dim" do 
  it "returns the dimention of the narray" do 
    Narray[[1,2],[3,4]].dim.should eq 2
  end 
end 

describe "shape" do 
  it "returns an narray with the shape (# elements at each axis)" do 
    Narray[1,2,3].shape.should eq Narray[3]
    Narray[[1,2],[1,2]].shape.should eq Narray[2,2]
    Narray[[1],[2],[3]].shape.should eq Narray[3,1]
  end 
end 


describe "stride" do 
  it "returns an narray with the stride (step size along axis)" do 
    Narray[1,2,3].stride.should eq Narray[1]
  end 
end


describe "to_unsafe" do 
  it "returns the start of the memory block" do 
    a = Narray[1,2,3]
    ptr = a.to_unsafe
    ptr.should be_a Pointer(Int32)
    ptr.value.should eq a[0]
    (ptr+1).value.should eq a[1]
    (ptr+2).value.should eq a[2]
  end 
end 

describe "empty?" do 
  it "returns true if there's no elements in the narray, false otherwise" do 
    Narray[1,2,3].empty?.should be_false 
    Narray(Int32).new.empty?.should be_true 
  end 
end 
