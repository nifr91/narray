require "./spec_helper.cr"


describe "construction of multi-dimentional arrays" do 

  it "3x1" do
    actual = Narray[ [1.0],
                     [2.0], 
                     [3]]

    actual.shape.should eq Narray[3,1]
    actual.to_s.should eq "[[1.0],[2.0],[3]]"
  end 

  it "1x3" do 
    actual = Narray[[1,2,3]]

    actual.shape.should eq Narray[1,3] 
    actual.to_s.should eq "[[1,2,3]]"
  end 

  it "3x3" do 
    actual = Narray[[1,2,3],
                    [4,5,6],
                    [7,8,9]]

    actual.shape.should eq Narray[3,3] 
    actual.to_s.should eq "[[1,2,3],[4,5,6],[7,8,9]]"
  end 

  it "1x2x3" do 
    actual = Narray[[[1,2,3],
                     [4,5,6]]]
    
    actual.shape.should eq Narray[1,2,3]
    actual.to_s.should eq "[[[1,2,3],[4,5,6]]]"
  end 


  it "2x1x3x1" do 
    actual = Narray[[[[1],
                      [2],
                      [3]]],
                    [[[4],
                      [5],
                      [6]]]]

    actual.shape.should eq Narray[2,1,3,1]
    actual.to_s.should eq "[[[[1],[2],[3]]],[[[4],[5],[6]]]]"
  end 

  it "0" do 
    actual = Narray[Int32]
    actual.shape.should eq Narray[0]
    actual.to_s.should eq "[]"
  end 

  it "1x1x1x1x1x1x1x1x1x1" do 
    actual = Narray[[[[[[[[[[1]]]]]]]]]]
    actual.shape.should eq Narray[1,1,1,1,1,1,1,1,1,1]
    actual.to_s.should eq "[[[[[[[[[[1]]]]]]]]]]"

  end 

end 
